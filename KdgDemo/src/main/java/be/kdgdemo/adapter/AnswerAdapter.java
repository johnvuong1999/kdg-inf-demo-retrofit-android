package be.kdgdemo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Ordering;

import java.util.List;

import be.kdgdemo.R;
import be.kdgdemo.model.Answer;
import butterknife.ButterKnife;
import butterknife.InjectView;

import static android.view.LayoutInflater.from;

/**
 * @author Jo Somers
 */
public class AnswerAdapter extends BaseAdapter {

    private final static Ordering<Answer> ANSWER_ORDERING = new Ordering<Answer>() {
        @Override
        public int compare(Answer left, Answer right) {
            return right.getModified().compareTo(left.getModified());
        }
    };

    private final Context context;

    private List<Answer> answers;

    public AnswerAdapter(
            final Context context,
            final List<Answer> answers) {

        this.context = context;
        setAnswers(answers);
    }

    public void setAnswers(final List<Answer> answers) {
        this.answers = ANSWER_ORDERING.sortedCopy(answers);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return answers.size();
    }

    @Override
    public Answer getItem(int position) {
        return answers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Answer question = getItem(position);

        ViewHolder viewHolder;
        if (convertView != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            convertView = from(context).inflate(R.layout.answer_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        viewHolder.modifiedTextView.setText(question.getModified().toString("dd-MM-yyyy hh:mm"));
        viewHolder.questionTextView.setText(question.getAnswer());

        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.modified)
        TextView modifiedTextView;

        @InjectView(R.id.question)
        TextView questionTextView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
