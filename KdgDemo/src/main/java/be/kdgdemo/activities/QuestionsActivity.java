package be.kdgdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.common.reflect.TypeToken;

import java.util.List;

import be.kdgdemo.R;
import be.kdgdemo.adapter.QuestionAdapter;
import be.kdgdemo.dialogs.AlertDialogFragment;
import be.kdgdemo.json.GsonHelper;
import be.kdgdemo.model.Question;
import be.kdgdemo.util.AlertDialogUtil;
import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.widget.AdapterView.OnItemClickListener;
import static android.widget.AdapterView.OnItemLongClickListener;

/**
 * @author Jo Somers
 */
public class QuestionsActivity extends BaseActivity implements OnItemClickListener,
        OnItemLongClickListener, Callback<List<Question>> {

    private static final String TAG = QuestionsActivity.class.getSimpleName();

    @InjectView(R.id.listview)
    ListView listView;

    @InjectView(R.id.empty)
    View emptyView;

    private QuestionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_questions);

        ButterKnife.inject(this);

        adapter = new QuestionAdapter(this);

        listView.setAdapter(adapter);
        listView.setEmptyView(emptyView);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshQuestions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_questions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                startActivity(new Intent(this, NewQuestionActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void success(
            final List<Question> questions,
            final Response response) {

        adapter.setQuestions(questions);
    }

    @Override
    public void failure(final RetrofitError error) {
        getVisualisation().showError(getString(R.string.tFailure));

        Log.e(TAG, error.toString());
    }

    @Override
    public void onItemClick(
            final AdapterView<?> parent,
            final View view,
            final int position,
            final long id) {

        final GsonHelper<Question> gsonMarshaller = new GsonHelper<Question>(new TypeToken<Question>() {
        }.getType());

        final Intent detailIntent = new Intent(this, QuestionsDetailActivity.class);
        detailIntent.putExtra("question", gsonMarshaller.toJson(adapter.getItem(position)));
        startActivity(detailIntent);
    }

    @Override
    public boolean onItemLongClick(
            final AdapterView<?> parent,
            final View view,
            final int position,
            final long id) {

        final Question question = adapter.getItem(position);

        final AlertDialogFragment.Callback questionCallback = new AlertDialogFragment.Callback() {
            @Override
            public void onPositiveButtonClicked() {
                deleteQuestion(position, question);
            }

            @Override
            public void onCancel() {
                // just dismiss, to nothing.
            }
        };

        AlertDialogUtil.showDialog(
                getFragmentManager(),
                TAG,
                questionCallback,
                getString(R.string.tQuestionDeleteTitle),
                getString(R.string.tQuestionDeleteMessage).replace("{question}", "'" + question.getQuestion() + "'"),
                getString(R.string.tQuestionDeletePositive),
                getString(R.string.tQuestionDeleteNegative));

        return true;
    }

    private void handleFailure(final Throwable throwable) {
        getVisualisation().showError(getString(R.string.tFailure));

        Log.e(TAG, throwable.toString());
    }

    private void refreshQuestions() {
        getService().getQuestions(this);
    }

    private void deleteQuestion(
            final int position,
            final Question question) {

        final Callback<Question> questionCallback = new Callback<Question>() {
            @Override
            public void success(
                    final Question question,
                    final Response response) {

                adapter.remove(position);
            }

            @Override
            public void failure(final RetrofitError error) {
                handleFailure(error);
            }
        };

        getService().deleteQuestion(question.getId(), questionCallback);
    }
}
